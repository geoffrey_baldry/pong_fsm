#ifndef PLAY_STATE
#define PLAY_STATE

#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_image.h>

#include "gamestate.h"



class Paddle;
class Ball;
class Court;
class LTimer;

class PlayState : public GameState {
public:
	void init(GameEngine* game);
	void clean_up(GameEngine* game);

	void pause();
	void resume();
	void reset();

	void input(GameEngine* game);
	void update(GameEngine* game);
	void render(GameEngine* game);

	static PlayState* Instance() { return &m_playstate; }

protected:
	PlayState() { }

private:
	static PlayState m_playstate;

	// Game objects
	Paddle* paddle1;
	Paddle* paddle2;
	Ball* ball;
	Court* court;

	// Timer
	//LTimer* timer;

	// Font textures
	SDL_Color white;
	TTF_Font* font_launch;
	TTF_Font* font_score;
	SDL_Texture* font_image_launch;
	SDL_Texture* font_image_exit;
	SDL_Texture* font_image_paused;
	SDL_Texture* font_image_p1score;
	SDL_Texture* font_image_p2score;
	SDL_Texture* font_image_winner;

	// Text position
	int launch_width, launch_height;
	int exit_width, exit_height;
	int paused_width, paused_height;
	int p1score_width, p1score_height;
	int p2score_width, p2score_height;
	int winner_width, winner_height;

	bool p1wins;
	bool p2wins;
	bool paused;
	bool game_over;
	bool exit;
};


#endif // !PLAY_STATE
