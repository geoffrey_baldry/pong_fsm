#ifndef GAME_ENGINE
#define GAME_ENGINE

#include <SDL.h>

#include <vector>

class GameState;

class LTimer;

class GameEngine {
public:
	GameEngine();

	void clean_up();

	void change_state(GameState* state);
	void push_state(GameState* state);
	void pop_state();

	void execute();
	void input();
	void update();
	void render();

	bool running() { return !exit; }
	void quit() { exit = true; }

	// Screen dimensions
	int width;
	int height;

	// Window and renderer
	SDL_Window* window;
	SDL_Renderer* renderer;

	// Timer used for FPS
	LTimer* timer;

	// DeWitters Game Loop Implementation variables - Constant game speed, Max FPS.
	const int TICKS_PER_SECOND = 50;
	const int SKIP_TICKS = 1000 / TICKS_PER_SECOND;
	const int MAX_FRAMESKIP = 10;

private:
	// Stack of states
	std::vector<GameState*> states;

	bool exit;

};

#endif // !GAME_ENGINE
