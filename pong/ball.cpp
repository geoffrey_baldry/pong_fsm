#include "ball.h"

#include <cmath>
#include <random>
#include <iostream>

namespace {
	std::random_device rd;
	std::mt19937 gen(rd());
}

Ball::Ball() {

	std::cout << "Ball object being created" << std::endl;

	status = READY;

	xPos = 0;
	yPos = 0;

	xVelocity = startVelocity;
	yVelocity = 0;

	ballRect.x = xPos;
	ballRect.y = yPos;
	ballRect.w = width;
	ballRect.h = height;

	hits = 0;
}

Ball::~Ball() {
	std::cout << "Ball object being destroyed" << std::endl;
}

void Ball::setPosition(int x, int y) {
	xPos = x;
	yPos = y;

	ballRect.x = xPos;
	ballRect.y = yPos;
}

void Ball::launch_ball() {
	std::uniform_int_distribution<int> xDir(0, 1);
	int xDirection = 1 + (-2)*(xDir(gen) % 2);  // Either 1 or -1

	xVelocity = xDirection * startVelocity;

	std::uniform_int_distribution<int> yDir(-3, 3); // Number from -3 to 3
	yVelocity = int(yDir(gen));

	status = IN_PLAY;
}

void Ball::rebound() {
	yVelocity = -yVelocity;
}

void Ball::move(GameEngine* game, Paddle* paddle1, Paddle* paddle2, Court* court) {

	yPos += yVelocity;
	xPos += xVelocity;

	if (xPos < 0) {
		court->paddle2_score += 1;
		court->score_changed = true;
		reset(game->width / 2 - width / 2, game->height / 2 - height / 2);
	}
	else if (xPos + width > game->width) {
		court->paddle1_score += 1;
		court->score_changed = true;
		reset(game->width / 2 - width / 2, game->height / 2 - height / 2);
	}
	else if (SDL_HasIntersection(&ballRect, &paddle1->getPaddle())) {
		hits++;
		xPos = 50 + paddle1->width;
		xVelocity = -xVelocity;
		yVelocity += paddle1->getVelocity() / 3; // Add some spin to ball if paddle moving
	}
	else if (SDL_HasIntersection(&ballRect, &paddle2->getPaddle())) {
		hits++;
		xPos = game->width - 50 - paddle2->width - width;
		xVelocity = -xVelocity;
		yVelocity += paddle2->getVelocity() / 3; // Add some spin to ball if paddle moving
	}

	if (yPos < 0) {
		yPos = 0;
		rebound();
	}
	else if (yPos > game->height - height) {
		yPos = game->height - height;
		rebound();
	}

	ballRect.x = xPos;
	ballRect.y = yPos;

	// If hits == 3, increase the xVelocity
	if (hits == 3) {
		xVelocity >= 0 ? ++xVelocity : --xVelocity;
		hits = 0;
	}
}

void Ball::reset(int x, int y) {
	// Set ball back to center
	xPos = x;
	yPos = y;

	// Set ball speed to 0
	xVelocity = 0;
	yVelocity = 0;

	// Reset hits to 0
	hits = 0;

	// Set ball READY to be launched
	status = READY;
}