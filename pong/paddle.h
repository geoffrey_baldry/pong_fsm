#ifndef PADDLE
#define PADDLE

#include <SDL.h>
#include "game_engine.h"

class Paddle {
public:
	Paddle();
	~Paddle();

	int maxVelocity;
	int height = 70;
	int width = 10;

	SDL_Rect getPaddle() { return paddleRect; }

	void setPosition(int x, int y);

	void onup() { curVelocity -= maxVelocity; }
	void offup() { curVelocity += maxVelocity; }
	void ondown() { curVelocity += maxVelocity; }
	void offdown() { curVelocity -= maxVelocity; }

	int getYPosition() { return yPos; }
	int getVelocity() { return curVelocity; }
	
	void move(GameEngine* game);

private:
	int xPos, yPos;
	int curVelocity;
	SDL_Rect paddleRect;
};

#endif // !PADDLE
