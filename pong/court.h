#ifndef COURT_H
#define COURT_H

#include <SDL.h>

class Court {
public:
	Court();
	~Court();

	SDL_Rect centerLine;

	SDL_Rect getCenterLine() { return centerLine; }

	int paddle1_score;
	int paddle2_score;

	int score_to_win;

	bool score_changed;

	int x, y, w, h;
};

#endif // !COURT_H
