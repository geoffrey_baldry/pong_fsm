#include "game_engine.h"
#include "gamestate.h"
#include "timer.h"

#include <iostream>

GameEngine::GameEngine() {

	std::cout << "Initializing GameEngine" << std::endl;

	SDL_Init(SDL_INIT_EVERYTHING);

	// Screen dimensions
	width = 640;
	height = 480;

	// Window and renderer
	window = SDL_CreateWindow("Geoff's Pong",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		width,
		height,
		SDL_WINDOW_SHOWN);

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	timer = new LTimer();

	exit = false;
}

void GameEngine::execute() {

	timer->start();
	Uint32 next_game_tick = timer->getTicks();
	int loops;


	// DeWitter's game loop, Fixed timestep, Maximum FPS
	while (!exit) {

		loops = 0;
		while (timer->getTicks() > next_game_tick && loops < MAX_FRAMESKIP) {
			input();
			update();
			
			next_game_tick += SKIP_TICKS;
			loops++;
		}

		render();
		
	}
	clean_up();
}

void GameEngine::clean_up() {

	std::cout << "Destroying GameEngine" << std::endl;

	// Clean up the current state
	while (!states.empty()) {
		states.back()->clean_up(this);
		states.pop_back();
	}

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	window = NULL;
	renderer = NULL;

	std::cout << "Destroyed GameEngine" << std::endl;

	SDL_Quit();
}

void GameEngine::change_state(GameState* state) {
	// Clean up the current state
	if (!states.empty()) {
		states.back()->clean_up(this);
		states.pop_back();
	}

	// Store and initialize the new state
	states.push_back(state);
	states.back()->init(this);
}

void GameEngine::push_state(GameState* state) {
	// Pause current state
	if (!states.empty())
		states.back()->pause();

	// Store and initialize the new state
	states.push_back(state);
	states.back()->init(this);
}

void GameEngine::pop_state() {
	// Pause current state
	if (!states.empty()) {
		states.back()->clean_up(this);
		states.pop_back();
	}

	// Resume previous state
	if (!states.empty())
		states.back()->resume();

}

void GameEngine::input() {
	// Let the state handle events
	states.back()->input(this);
}

void GameEngine::update() {
	// Let the state handle updates
	states.back()->update(this);
}

void GameEngine::render() {
	// Let the state handle rendering
	states.back()->render(this);
}
