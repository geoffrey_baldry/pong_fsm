#include "introstate.h"
#include "menustate.h"
#include "utilities.h"

#include <iostream>

IntroState IntroState::m_introstate;

void IntroState::init(GameEngine* game) {
	std::cout << "Initialized IntroState" << std::endl;

	bool result = true;

	logo = load_texture("pong_logo.png", game->renderer);
	if (!logo) {
		printf("IMG_LoadTexture: %s\n", IMG_GetError());
	}

	exit = false;
	next_state = false;
}

void IntroState::clean_up(GameEngine* game) {

	std::cout << "Destroying IntroState" << std::endl;

	IMG_Quit();
}

void IntroState::pause() {}
void IntroState::resume() {}
void IntroState::reset() {}

void IntroState::input(GameEngine* game) {
	SDL_Event event;
	while (SDL_PollEvent(&event)) {
		// Clicking 'x' or pressing F4
		if (event.type == SDL_QUIT)
			exit = true;

		// Key is pressed
		if (event.type == SDL_KEYDOWN) {
			switch (event.key.keysym.sym) {
			case SDLK_ESCAPE:
				exit = true;
				break;
			case SDLK_SPACE:
				next_state = true;
			default:
				break;
			}
		}
	}
}

void IntroState::update(GameEngine* game) {
	if (exit) {
		game->quit();
	}
	if (next_state) {
		next_state = false;
		//game->push_state(MenuState::Instance());
		game->change_state(MenuState::Instance());
	}
}

void IntroState::render(GameEngine* game) {
	// Clear screen
	SDL_SetRenderDrawColor(game->renderer, 0, 0, 0, 1);
	SDL_RenderClear(game->renderer);

	render_logo(game);

	// Swap buffers
	SDL_RenderPresent(game->renderer);
}

void IntroState::render_logo(GameEngine* game) {
	int logo_width, logo_height;
	SDL_QueryTexture(logo, nullptr, nullptr, &logo_width, &logo_height);
	int x = game->width / 2 - logo_width / 2;
	int y = game->height / 2 - logo_width / 2;

	render_texture(logo, game->renderer, x, y);
}