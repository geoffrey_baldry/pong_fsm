#include <SDL.h>
#include "court.h"
#include <iostream>

Court::Court() {

	std::cout << "Court object being created" << std::endl;
	
	paddle1_score = 0;
	paddle2_score = 0;

	score_to_win = 5;

	score_changed = false;

	centerLine.x = 0;
	centerLine.y = 0;
	centerLine.h = 30;
	centerLine.w = 3;
}

Court::~Court() {
	std::cout << "Court object being destroyed" << std::endl;
	
}

