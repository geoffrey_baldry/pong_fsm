#ifndef INTRO_STATE
#define INTRO_STATE

#include <SDL.h>

#include "gamestate.h"

class IntroState : public GameState {
public:
	void init(GameEngine* game);
	void clean_up(GameEngine* game);

	void pause();
	void resume();
	void reset();

	void input(GameEngine* game);
	void update(GameEngine* game);
	void render(GameEngine* game);

	static IntroState* Instance() { return &m_introstate; }

	void render_logo(GameEngine* game);

protected:
	IntroState() {}

private:
	static IntroState m_introstate;

	bool next_state;
	bool exit;

	SDL_Texture* logo;

};

#endif // !INTRO STATE