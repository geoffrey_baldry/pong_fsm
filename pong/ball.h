#ifndef BALL
#define BALL

#include <SDL.h>
#include "game_engine.h"
#include "paddle.h"
#include "court.h"

class Ball {
public:
	Ball();
	~Ball();

	// Ball status
	enum Status { READY, LAUNCH, IN_PLAY };
	Status status;

	SDL_Rect getBall() { return ballRect; }

	int height = 10;
	int width = 10;
	int startVelocity = 3;
	int hits;
	void setPosition(int x, int y);
	void move(GameEngine* game, Paddle* paddle1, Paddle* paddle2, Court* court);
	void rebound();
	void reset(int x, int y);
	void launch_ball();

private:
	int xPos, yPos;
	int xVelocity, yVelocity;
	SDL_Rect ballRect;
};

#endif // !BALL
