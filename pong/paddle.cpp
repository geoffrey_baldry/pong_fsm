#include "paddle.h"
#include <iostream>


Paddle::Paddle() {
	
	std::cout << "Paddle object being created" << std::endl;

	maxVelocity = 10;
	curVelocity = 0;

	xPos = 0;
	yPos = 0;

	paddleRect.x = xPos;
	paddleRect.y = yPos;
	paddleRect.w = width;
	paddleRect.h = height;
}

Paddle::~Paddle() {
	std::cout << "Paddle object being destroyed" << std::endl;
}

void Paddle::setPosition(int x, int y) {
	xPos = x;
	yPos = y;

	paddleRect.x = xPos;
	paddleRect.y = yPos;
}

void Paddle::move(GameEngine* game) {
	// Move the paddle up or down
	yPos += curVelocity;

	// If the paddle hit the boundary, stop
	if (yPos < 0) {
		// Move Back
		yPos = 0;
	}
	else if (yPos + height > game->height){
		yPos = game->height - height;
	}

	paddleRect.y = yPos;
}