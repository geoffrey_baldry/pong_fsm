#include "playstate.h"
#include "menustate.h"

#include "game_engine.h"
#include "utilities.h"
#include "paddle.h"
#include "ball.h"
#include "court.h"

#include <iostream>

PlayState PlayState::m_playstate;

void PlayState::init(GameEngine* game) {

	std::cout << "Initializing PlayState" << std::endl;

	// Game objects
    paddle1 = new Paddle();
	paddle2 = new Paddle();
	ball = new Ball();
	court = new Court();

	// Place paddles into correct positions
	paddle1->setPosition(50, game->height / 2 - paddle1->height / 2);
	paddle2->setPosition(game->width - 50 - paddle2->width, game->height / 2 - paddle2->height / 2);

	// Place ball in centre of court
	ball->setPosition(game->width / 2 - ball->width / 2, game->height / 2 - ball->height / 2);

	// Prepare fonts
	TTF_Init();
	white = { 255, 255, 255 };
	font_launch = TTF_OpenFont("resources/fonts/ARCADE_I.ttf", 16);
	if (!font_launch) {
		printf("TTF_OpenFont: %s\n", TTF_GetError());
	}
	font_score = TTF_OpenFont("resources/fonts/ARCADE_I.ttf", 32);
	if (!font_score) {
		printf("TTF_OpenFont: %s\n", TTF_GetError());
	}

	// Create texture from font
	font_image_launch = render_text("Press Space to launch!", white, font_launch, game->renderer);
	font_image_exit = render_text("Press Space to Exit!", white, font_launch, game->renderer);
	font_image_paused = render_text("Paused...", white, font_launch, game->renderer);
	font_image_p1score = render_text(std::to_string(court->paddle1_score), white, font_score, game->renderer);
	font_image_p2score = render_text(std::to_string(court->paddle2_score), white, font_score, game->renderer);

	// Text position
	SDL_QueryTexture(font_image_launch, nullptr, nullptr, &launch_width, &launch_height);
	SDL_QueryTexture(font_image_exit, nullptr, nullptr, &exit_width, &exit_height);
	SDL_QueryTexture(font_image_paused, nullptr, nullptr, &paused_width, &paused_height);
	SDL_QueryTexture(font_image_p1score, nullptr, nullptr, &p1score_width, &p1score_height);
	SDL_QueryTexture(font_image_p2score, nullptr, nullptr, &p2score_width, &p2score_height);

	// Set win flags
	p1wins = false;
	p2wins = false;

	paused = false;
	game_over = false;
	exit = false;
}

void PlayState::clean_up(GameEngine* game) {

	std::cout << "Destroying PlayState" << std::endl;

	// Free game objects
	delete paddle1;
	delete paddle2;
	delete ball;
	delete court;

	// Destroy textures
	SDL_DestroyTexture(font_image_launch);
	SDL_DestroyTexture(font_image_p1score);
	SDL_DestroyTexture(font_image_p2score);
	SDL_DestroyTexture(font_image_winner);
	SDL_DestroyTexture(font_image_exit);
	SDL_DestroyTexture(font_image_paused);

	// Free SDL
	IMG_Quit();
	TTF_Quit();
}

void PlayState::pause() {
	paused = true;
}

void PlayState::resume() {
	paused = false;
}

void PlayState::reset() {

}

void PlayState::input(GameEngine* game) {
	SDL_Event event;
	while (SDL_PollEvent(&event)) {
		// Clicking 'x' or pressing F4
		if (event.type == SDL_QUIT) {
			exit = true;
		}

		// Key is pressed
		if (event.type == SDL_KEYDOWN) {
			// Pause/Resume
			if (event.key.keysym.sym == SDLK_p) {
				if (paused) {
					std::cout << "Unpausing" << std::endl;
					resume();
				}
				else {
					std::cout << "Pausing" << std::endl;
					pause();
				}
			}
			// Hit Esc to exit
			else if (event.key.keysym.sym == SDLK_ESCAPE) {
				exit = true;
			}
			// Toggle FullScreen
			else if (event.key.keysym.sym == SDLK_F11) {
				int flags = SDL_GetWindowFlags(game->window);
				if (flags & SDL_WINDOW_FULLSCREEN) {
					SDL_SetWindowFullscreen(game->window, 0);
				}
				else {
					SDL_SetWindowFullscreen(game->window, SDL_WINDOW_FULLSCREEN);
				}
			}

			if (!paused) {
				if (event.key.keysym.sym == SDLK_SPACE) {
					if (game_over) {
						game_over = false;
						game->change_state(MenuState::Instance()); // Exit the level, back to MenuState
					}
					if (ball->status == ball->READY) {
						ball->status = ball->LAUNCH;
					}
				}

				if (event.key.repeat == 0) {
					switch (event.key.keysym.sym) {
					case SDLK_q:
						paddle1->onup(); break;
					case SDLK_a:
						paddle1->ondown(); break;
					case SDLK_UP:
						paddle2->onup(); break;
					case SDLK_DOWN:
						paddle2->ondown(); break;
					default:
						break;
					}
				}
			}
		}

		if (event.type == SDL_KEYUP) {
			if (!paused && event.key.repeat == 0) {		
				switch (event.key.keysym.sym) {
				case SDLK_q:
					paddle1->offup(); break;
				case SDLK_a:
					paddle1->offdown(); break;
				case SDLK_UP:
					paddle2->offup(); break;
				case SDLK_DOWN:
					paddle2->offdown(); break;
				default:
					break;
				}
			}
		}
	}
}

void PlayState::update(GameEngine* game) {
	if (exit) {
		game->quit();
	}

	if (game_over || paused) {
		return;
	}

	paddle1->move(game);
	paddle2->move(game);

	// Launch ball if necessary
	if (ball->status == ball->READY) {
		return;
	}
	else if (ball->status == ball->LAUNCH) {
		ball->launch_ball();
	}
	else if (ball->status == ball->IN_PLAY) {
		ball->move(game, paddle1, paddle2, court);
	}

	if (court->paddle1_score == court->score_to_win) {
		p1wins = true;
		game_over = true;
	}
	else if (court->paddle2_score == court->score_to_win) {
		p2wins = true;
		game_over = true;
	}
}

void PlayState::render(GameEngine* game) {
	// Clear screen
	SDL_SetRenderDrawColor(game->renderer, 0, 0, 0, 255);
	SDL_RenderClear(game->renderer);

	// Set colour to white for ball and paddles
	SDL_SetRenderDrawColor(game->renderer, 255, 255, 255, 255);

	// Render court center line
	court->centerLine.x = game->width / 2 - court->centerLine.w / 2;
	int num_dashes = 10;
	for (int i = 0; i < num_dashes; i++) {
		SDL_RenderFillRect(game->renderer, &court->getCenterLine());
		court->centerLine.y += ( ( (game->height) - (num_dashes * court->centerLine.h) ) / (num_dashes - 1) ) + court->centerLine.h;
	}
	court->centerLine.y = 0;

	// Render paddles
	SDL_RenderFillRect(game->renderer, &paddle1->getPaddle());
	SDL_RenderFillRect(game->renderer, &paddle2->getPaddle());

	// Render ball
	SDL_RenderFillRect(game->renderer, &ball->getBall());

	// Prepare score textures if score has updated
	if (court->score_changed) {
		font_image_p1score = render_text(std::to_string(court->paddle1_score), white, font_score, game->renderer);
		font_image_p2score = render_text(std::to_string(court->paddle2_score), white, font_score, game->renderer);
		SDL_QueryTexture(font_image_p1score, nullptr, nullptr, &p1score_width, &p1score_height);
		SDL_QueryTexture(font_image_p2score, nullptr, nullptr, &p2score_width, &p2score_height);
		
		court->score_changed = false;
	}

	// Render scores
	render_texture(font_image_p1score, game->renderer, (game->width / 2) - p1score_width - 20, 20);
	render_texture(font_image_p2score, game->renderer, (game->width / 2) + 20, 20);
	
	if (p1wins) {
		font_image_winner = render_text("Player 1 Wins!", white, font_score, game->renderer);
		SDL_QueryTexture(font_image_winner, nullptr, nullptr, &winner_width, &winner_height);
		p1wins = false;
	}
	else if (p2wins) {
		font_image_winner = render_text("Player 2 Wins!", white, font_score, game->renderer);
		SDL_QueryTexture(font_image_winner, nullptr, nullptr, &winner_width, &winner_height);	
		p2wins = false;
	}

	// Render required text to screen
	if (game_over) {
		render_texture(font_image_winner, game->renderer, game->width / 2 - winner_width / 2, (game->height / 2) + 100);
		render_texture(font_image_exit, game->renderer, (game->width - exit_width) / 2, (game->height - 50));
	} else if (ball->status == ball->READY) {
		render_texture(font_image_launch, game->renderer, (game->width - launch_width) / 2, (game->height - 50));
	}

	if (paused) {
		render_texture(font_image_paused, game->renderer, (game->width - paused_width) / 2, (game->height - 150));
	}

	// Swap buffers
	SDL_RenderPresent(game->renderer);
}